grunt-init-aborrmann
====================

This is the starting template for new grunt projects, containing commonly used libraries and a setup for quick JS development.

Getting started
---------------

Make sure grunt-cli is installed

	sudo npm install -g grunt-cli

Make sure grunt-init is installed

    sudo npm install -g grunt-init

Clone this repo to your `grunt-init` directory

    git clone git@bitbucket.org:aborrmann/grunt-init-aborrmann.git ~/.grunt-init/aborrmann-project

Then get your project started by initializing from your project directory:

    grunt-init aborrmann-project
    npm install
    grunt --force
    grunt watch

Maybe use bower to install any extra JS libs (install with `sudo npm -g install bower`)

	bower install jquery