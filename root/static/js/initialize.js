/*
 * {%= title %} by {%= author_name %}
 * {%= homepage %}
 *
 * Created on {%= grunt.template.today('yyyy') %}
  */

(function () {
    var global = this;

    // Map dependancies to local variables
    var $ = global.jQuery;


    // Constructor
    // ===========

    var Project = (global.Project || (global.Project = { }));

    var Core = Project.Core = function (options) {
        var defaults = { };
        this.config = $.extend(true, defaults, options || { });
        this._initialize();
    };


    Core.prototype._initialize = function () {
        this._initializePage();
    };


    Core.prototype._initializePage = function () {
        var classnames = $('body').prop('class').split(' ');
        var index = classnames.length;

        if (index > 1) {
            while(index--) {
                switch (classnames[index]) {
                    case 'home-page':
                    case 'detail-page':
                        // Execute page specific javascript
                        break;

                    case 'contact-page':
                        break;
                }
            }
        }
    };


}).call(this);