'use strict';

exports.description = 'Initialize a basic project directory structure';

exports.template = function(grunt, init, done) {

    var path = require('path');

    init.process(
        { type: 'project'},
        [
            init.prompt('name'),
            init.prompt('title'),
            init.prompt(
                'repository',
                function(value, data, done) {
                    done(null, 'https://bitbucket.org/aborrmann/' + data.name);
                }
            ),
            init.prompt('homepage')
        ],
        function(err, props) {
            props.author_name = 'Adrian Borrmann';
            props.author_url = 'http://www.adrianborrmann.com';

            var files = init.filesToCopy(props);
            init.copyAndProcess(files, props, { noProcess: 'libs/**' });

            props.devDependencies = {
                "bower": "*",
                "grunt": "~0.4.1",
                "grunt-contrib-clean": "~0.5.0",
                "grunt-contrib-concat": "~0.3.0",
                "grunt-contrib-less": "~0.7.0",
                "grunt-contrib-connect": "~0.5.0",
                "grunt-contrib-jshint": "~0.6.4",
                "grunt-contrib-jst": "~0.5.1",
                "grunt-contrib-uglify": "~0.2.4",
                "grunt-contrib-watch": "~0.5.3",
                "grunt-exec": "~0.4.2"
            };

            init.writePackageJSON('package.json', props);

            done();
        }
    );
}